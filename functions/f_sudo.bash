# F_SUDO
# A simple function to ask for sudo user

function f_sudo_usage {
  printf "F_SUDO\n"
  printf "A simple function to ask for sudo user.\n"
  printf "\n"
  printf "USAGE:\n"
  printf "  f_sudo [OPTIONS]\n"
  printf "\n"
  printf "OPTIONS:\n"
  printf "  -h\n"
  printf "    Print this help message.\n"
}

function f_sudo {
  OPTIND=1 # Precation: reset getopts' $OPTIND in case of multiple calls

  # Check the input arguments
  while getopts ":h" arg; do
    case $arg in
      h) # Help
        f_sudo_usage
        return 0
        ;;
      :) # Argument
        printf "Option -%s requires an argument \n" "$OPTARG"
        f_sudo_usage
        return 1
        ;;
      \?) # Error
        printf "Invalid option: -%s\n" "$OPTARG"
        f_sudo_usage
        return 1
        ;;
      *) # Other
        f_sudo_usage
        return 1
        ;;
    esac
  done

  # Prompt sudo password
  if [ $EUID != 0 ]; then
    printf "This program has to be run with superuser privileges (the SUDO mode).\n"
    printf "Exiting ...\n"
    return 1
  else
    return 0
  fi
}
