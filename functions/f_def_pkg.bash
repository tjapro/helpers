# # F_TEMPLATE
# # A simple file to show how to begin with a ".lib" file
#
# readonly G_TEMPLATE="template"
# readonly G_MESSAGE="no message given"
# readonly G_PACKAGES=("pkg" "apk" "apt" "apt-get" "dnf" "yum")
# #                   |termux|alpine|debian       |  redhat   |
#
# function f_template_usage {
#   printf "F_TEMPLATE\n"
#   printf "A simple file to show how to begin with a '.bash' library file.\n"
#   printf "\n"
#   printf "USAGE:\n"
#   printf "  f_template [OPTIONS] [MESSAGE]\n"
#   printf "\n"
#   printf "OPTIONS:\n"
#   printf "  -h\n"
#   printf "    Print this help message.\n"
#   printf "\n"
#   printf "ARGS:\n"
#   printf "  <MESSAGE>\n"
#   printf "    The text to print in the logs file.\n"
# }
#
# function f_def_pkg {
#   for p in "$G_PACKAGES"
# }
#
#
# function f_template {
#   local l_message=""
#
#   OPTIND=1 # Precation: reset getopts' $OPTIND in case of multiple calls
#
#   # Check the input arguments
#   while getopts ":h" arg; do
#     case "$arg" in
#       h) # Help
#         f_template_usage
#         return 0
#         ;;
#       :) # Argument
#         printf "An argument is required for option '-%s'\n" "$OPTARG"
#         f_template_usage
#         return 1
#         ;;
#       \?) # Error
#         printf "Invalid option: '-%s'\n" "$OPTARG"
#         f_template_usage
#         return 1
#         ;;
#       *) # Other
#         printf "Internal error\n"
#         f_template_usage
#         return 1
#         ;;
#     esac
#   done
#
#   # Get the message (the last argument)
#   shift $(( OPTIND - 1 ))
#   l_message="$*"
#   if [[ $l_message = "" ]]; then
#     l_message="$G_MESSAGE"
#   fi
#
#   # Write the message in the file
#   printf "%s :: %s" "$G_TEMPLATE" "$l_message"
#
#   return 0
# }
