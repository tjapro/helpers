# F_LINE
# A simple function that writes lines

function f_line_usage {
  printf "F_LINE\n"
  printf "A simple function that writes lines.\n"
  printf "\n"
  printf "USAGE:\n"
  printf "  f_line [OPTIONS]\n"
  printf "\n"
  printf "OPTIONS:\n"
  printf "  -h\n"
  printf "    Print this help message.\n"
  printf "  -t <character>\n"
  printf "    Sets the character of the line. The default is '-'.\n"
  printf "    The types can be anything you want,\n"
  printf "     but the most usual are: - , = , . , \* , + , _ , \> , \<\n"
  printf "\n"
  printf "BUGS:\n"
  printf "  Attention to escape some characters, e.g: * -> \*"
  printf "\n"
}

function f_line {
  local l_character="-"
  local l_line=""

  OPTIND=1 # Precation: reset getopts' $OPTIND in case of multiple calls

  # Check the input arguments
  while getopts ":ht:" arg; do
    case "$arg" in
      h) # Help
        f_line_usage
        return 0
        ;;
      t) # Type
        l_character="$OPTARG"
        ;;
      :) # Argument
        printf "An argument is required for option '-%s'\n" "$OPTARG"
        f_line_usage
        return 1
        ;;
      \?) # Error
        printf "Invalid option: '-%s'\n" "$OPTARG"
        f_line_usage
        return 1
        ;;
      *) # Other
        printf "Internal error\n"
        f_line_usage
        return 1
        ;;
    esac
  done

  # Clean options
  shift $(( OPTIND - 1 ))

  for (( i = 0; i < $(tput cols); i++ )); do
    l_line+="$l_character"
  done

  printf "%s\n" "$l_line"

  return 0
}
