# F_LOG
# A simple function that writes logs in a file

readonly G_LOG_DIR="./logs/"
readonly G_LOG_EXT=".log"
readonly true=0
readonly false=1

function f_log_usage {
  printf "F_LOG\n"
  printf "A simple function that writes logs in a file and/or in the shell.\n"
  printf "If nothing is passed a default message is written.\n"
  printf "\n"
  printf "USAGE:\n"
  printf "  f_log [OPTIONS] [MESSAGE] [ARGUMENTS]\n"
  printf "\n"
  printf "OPTIONS:\n"
  printf "  -h\n"
  printf "    Print this help message.\n"
  printf "  -d\n"
  printf "    Dry-run; equals to -e -s, i.e., no output with newline.\n"
  printf "  -e\n"
  printf "    Output the string without other info and do not write it to file.\n"
  printf "  -n\n"
  printf "    Turn off the auto newline (only works on shell output).\n"
  printf "  -s\n"
  printf "    Silent the shell output (only output to file).\n"
  printf "  -T\n"
  printf "    No time printed in shell (output with time to file).\n"
  printf "  -f <name>\n"
  printf "    Creates a log file with that name.\n"
  printf "  -t <type>\n"
  printf "    Sets the type of the message. The default is log.\n"
  printf "    The types can be: log, info, warning, error.\n"
  printf "\n"
  printf "ARGS:\n"
  printf "  <MESSAGE>\n"
  printf "    The text to print in the logs file and shell output.\n"
  printf "  <ARGUMENTS>\n"
  printf "    See how to use it in \`printf\` documentation.\n"
  printf "\n"
  printf "BUGS:\n"
  printf "  For bugs see first \`printf\` program."
  printf "\n"
}

function f_log {
  local original_IFS=$IFS
  IFS=" "
  OPTIND=1 # Precation: reset getopts' $OPTIND in case of multiple calls
  local l_empty=$false
  local l_newline=$true
  local l_silent=$false
  local l_time=$true
  local l_filename=""
  local l_type="LOG    "
  local l_default_message="no message given"
  local l_input_message=""
  local l_shell_message=""
  local l_file_message=""

  # Check the input arguments
  while getopts ":hdensTf:t:" opt; do
    case "$opt" in
      h) # Help
        f_log_usage
        return 0
        ;;
      d) # Dry-run; equals to -e -s
        l_empty=$true
        l_silent=$true
        l_time=$false
        ;;
      e) # Empty
        l_empty=$true
        l_time=$false
        ;;
      n) # No newline
        l_newline=$false
        ;;
      s) # Silent
        l_silent=$true
        ;;
      T) # No time
        l_time=$false
        ;;
      f) # Filename
        l_filename="$OPTARG"
        ;;
      t) # Check the type
        case "$OPTARG" in
          log)
            l_type="LOG    "
            ;;
          info)
            l_type="INFO   "
            ;;
          warning)
            l_type="WARNING"
            ;;
          error)
            l_type="ERROR  "
            ;;
          *)
            printf "The argument '%s' is invalid for option '-t'\n" "$OPTARG"
            f_log_usage
            return 1
            ;;
        esac
        ;;
      :) # Argument
        printf "An argument is required for option '-%s'\n" "$OPTARG"
        f_log_usage
        return 1
        ;;
      \?) # Error
        printf "Invalid option: '-%s'\n" "$OPTARG"
        f_log_usage
        return 1
        ;;
      *) # Other
        printf "Internal error\n"
        f_log_usage
        return 1
        ;;
    esac
  done

  # Clean options; get the message and substituion strings
  shift $(( OPTIND - 1 ))

  # Get the message and leave the rest ($@)
  l_input_message="$1"; shift

  # Create the file
  if [ -z "$l_filename" ]; then
    # Source: https://stackoverflow.com/questions/1401482/yyyy-mm-dd-format-date-in-shell-script
    printf -v l_filename '%(%Y-%m-%d)T' -1
  fi
  touch "${G_LOG_DIR}${l_filename}${G_LOG_EXT}"

  # Create a default message if needed
  if [ -z "$l_input_message" ] && [ "$l_empty" = $false ] && [ "$l_silent" = $false ]; then
    l_input_message="$l_default_message"
  fi

  # Prepare the strings ...
  # ... Add time
  printf -v l_file_message "%(%H:%M:%S)T :: " -1
  if [ "$l_time" = $true ]; then
    l_shell_message+="$l_file_message"
  fi

  # ... Add type
  printf -v l_file_message "%s%s :: " "$l_file_message" "$l_type"
  if [ "$l_empty" = $false ]; then
    printf -v l_shell_message "%s%s :: " "$l_shell_message" "$l_type"
  fi

  # ... Add the message
  printf -v l_file_message "%s%s" "$l_file_message" "$l_input_message"
  printf -v l_shell_message "%s%s" "$l_shell_message" "$l_input_message"

  # ... Add the newline
  printf -v l_file_message "%s\n" "$l_file_message"
  if [ "$l_newline" = $true ]; then
    printf -v l_shell_message "%s\n" "$l_shell_message"
  fi

  # Write the string to the shell, if is not silent
  # $@ => arguments
  # shellcheck disable=SC2059
  # See: https://github.com/koalaman/shellcheck/wiki/SC2059#exceptions
  if [ "$l_silent" = $false ]; then
    printf "$l_shell_message" "$@"
  fi

  # Write the string to the file, if is not -e flag
  # Source: https://stackoverflow.com/questions/1401482/yyyy-mm-dd-format-date-in-shell-script
  # shellcheck disable=SC2059
  # See: https://github.com/koalaman/shellcheck/wiki/SC2059#exceptions
  if [ "$l_empty" = $false ]; then
    printf "${l_file_message}" "$@" >> "${G_LOG_DIR}${l_filename}${G_LOG_EXT}"
  fi

  IFS=$original_IFS

  return 0
}
