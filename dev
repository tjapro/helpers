#!/usr/bin/env bash

# TIAGO PROGRAMMER HELPERS
# Development script

# Bash Variables
# Ativate or not the erros (1=activated)
OPTERR=1
# Debugging variables
# set -o errexit -> do not activate this, because f_test_entr_version will crash
# set -o xtrace
# set -o nounset
# set -o pipefail -> do not activate this, because f_test_entr_version will crash

# Source helpers
source ./functions/f_line.bash
source ./functions/f_log.bash

# Global Variables
readonly G_DEPS="entr shellcheck ./libs/bats-core/bin/bats"
readonly G_ENTR_VERSION="4.5"
readonly G_SHELLCHECK_VERSION="0.7.0"
readonly G_BATS_VERSION="1.2.0"

G_WATCH="false"

# Helper Functions

# Output Usage
function f_usage {
  f_log -e "TIAGO PROGRAMMER HELPERS DEVELOPMENT"
  f_log -e "A simple file to help developing this project."
  f_log -e
  f_log -e "USAGE:"
  f_log -e "  %s [OPTIONS]" "$0"
  f_log -e
  f_log -e "OPTIONS:"
  f_log -e "    Without any option, this script runs once."
  f_log -e "  -h"
  f_log -e "    Show this help."
  f_log -e "  -i"
  f_log -e "    Install the hooks."
  f_log -e "  -t"
  f_log -e "    Only test once the files, without pre-verification."
  f_log -e "  -w"
  f_log -e "    Watch for changes and test them."
}

# Compare Version
# Compare the first version (x.x.x format) against second one.
# Returns 0 if $1 >= $2, 1 otherwise.
# In error returns 2.
# Sources:
#  https://unix.stackexchange.com/questions/285924/how-to-compare-a-programs-version-in-a-shell-script
#  https://stackoverflow.com/questions/4023830/how-to-compare-two-strings-in-dot-separated-version-format-in-bash
# Test table:
#  req   | get   | test  | res
# -------+-------+-------+-----------
#  2.0.0 | 1.0.0 | 1.0.0 | true  = 0
#  1.0.0 | 2.0.0 | 1.0.0 | false = 1
#  0.5.3 | 0.5.3 | 0.5.3 | true  = 0
function f_compare_version {
  local required_v="$1"
  local getted_v="$2"

  # First: check if equal
  if [ "$required_v" = "$getted_v" ]; then
    return 0;
  fi

  # Second: check if greater or lesser
  local test_v
  test_v="$(printf "%s\n%s" "$required_v" "$getted_v" | sort -V | head -n 1)"
  case $test_v in
    $getted_v) return 0 ;;
    $required_v) return 1 ;;
    *) return 1 ;;
  esac
}

# Output the test result
# If the test result is 0, output only the message
# If the test result is 1, output the message and return 1
function f_test_result {
  local value="$1"

  if [ "$value" = "0" ]; then
    f_log -e "ok"
    f_log -s "ok"
  else
    f_log -e "nok"
    f_log -s "nok"
    return 1
  fi
}

# Test Functions

function f_test {
  # Files
  local files=()
  files+=("dev")
  files+=("functions/*")
  files+=(".hooks/*")

  # Check the typing
  f_log -e
  f_log -T -t info "Running ShellCheck ... "

  # shellcheck disable=SC2068
  # Here we want globbing the individual elements.
  if shellcheck -f diff ${files[@]}; then
    shellcheck -f diff ${files[@]} | git am --signoff
  fi

  # shellcheck disable=SC2068
  # Here we want globbing the individual elements.
  if shellcheck ${files[@]}; then
    f_log -e "All ok"
    f_log -s "All ok"

    # Make the tests
    f_log -e
    f_log -T -t info "Running Bats ..."
    f_log -T -t info "Total: %s tests" "$(./libs/bats-core/bin/bats --count tests/*)"

    ./libs/bats-core/bin/bats --recursive --timing tests/*

  else
    f_log -e "Not ok"
    f_log -s "Not ok"
  fi
}

function f_test_deps {
  local DEPS_MISSED=""
  local DNF_STRING="sudo dnf install -y"
  local BATS_MISSED="false"

  # If there isn't the dependency the $DEPS_MISSED will be populated.
  for DEP in $G_DEPS; do
    if [ ! "$(command -v "$DEP")" ]; then
      DEPS_MISSED="${DEP} ${DEPS_MISSED}"
    fi
  done

  # Error if $DEPS_MISSED is populated.
  if [ "$DEPS_MISSED" = "" ]; then
    return 0
  else
    for DEP in $DEPS_MISSED; do
      f_log -t error "The '%s' program is required but was not found.\n > Install '%s' first and then run this script again." "${DEP}" "${DEP}"
      if [[ ! $DEP =~ bats ]]; then
        DNF_STRING="${DNF_STRING} $DEP"
      else
        BATS_MISSED="true"
      fi
    done
    if [ "$(command -v "dnf")" ]; then
      f_log -t error "In your system you can run: %s" "${DNF_STRING}"
    fi
    if [ $BATS_MISSED = "true" ]; then
      f_log -t error "After, install bats runnig: git submodule init && git submodule update"
    fi
    return 1
  fi
}

function f_test_entr_version {
  local version
  version="$(entr 2>&1 | cut -d ' ' -f 2 | head -n 1)"
  f_compare_version "$G_ENTR_VERSION" "$version"
  return $?
}

function f_test_shellcheck_version {
  local version
  version="$(shellcheck --version | cut -d ' ' -f 2 | head -n 2 | tail -n 1)"
  f_compare_version "$G_SHELLCHECK_VERSION" "$version"
  return $?
}

function f_test_bats_version {
  local version
  version="$(./libs/bats-core/bin/bats --version | cut -d ' ' -f 2 | head -n 1)"
  f_compare_version "$G_BATS_VERSION" "$version"
  return $?
}

# Watch Function
function f_watch {
  find ./**/* | entr -cd -s './dev -t'
}

# Install Function
function f_install {
  local hooks=()
  hooks+=("commit-msg")
  hooks+=("pre-commit")
  hooks+=("prepare-commit-msg")

  if [ -d ".git" ]; then
    for h in "${hooks[@]}"; do
      if ! [ -L ".git/hooks/${h}" ]; then ln -s "../../.hooks/${h}" ".git/hooks/${h}"; fi
    done
  fi
}

# Build Function
# Creates a `build` folder with the bash file.
# Sources:
#  https://stackoverflow.com/questions/37531927/replacing-source-file-with-its-content-and-expanding-variables-in-bash
# function f_build {
#
#
#   while read line; do
#     if [[ "$line" =~ (\.|source)\s+.+ ]]; then
#         file="$(echo $line | cut -d' ' -f2)"
#         echo "$(cat $file)"
#     else
#       echo "$line"
#     fi
# done < "$1"
# }

# MAIN

function main {
  local original_IFS=$IFS
  IFS=" "
  OPTIND=1 # Precation: reset getopts' $OPTIND in case of multiple calls

  while getopts ":hitw" arg; do
    case $arg in
      h) # Help
        f_usage
        return 0
        ;;
      i) # Install
        f_install
        return 0
        ;;
      t)
        f_test
        return 0
        ;;
      w)
        G_WATCH="true"
        ;;
      \?) # Error
        f_log -e "Invalid option: -%s" "$OPTARG"
        f_usage
        return 1
        ;;
      :) # Argument
        f_log -e "Option -%s requires an argument" "$OPTARG"
        f_usage
        return 1
        ;;
      *) # Other
        f_usage
        return 1
        ;;
    esac
  done

  f_line -t "="
  f_log -e
  f_log -e "TIAGO PROGRAMMER HELPERS"
  f_log -s -t info "TIAGO PROGRAMMER HELPERS"
  f_log -e
  f_line -t "="
  f_log -e
  f_log -Tn -t info "Testing dependencies ... "
  f_test_deps; f_test_result "$?" || return 1
  f_log -Tn -t info "Testing Entr version ... "
  f_test_entr_version; f_test_result "$?" || return 1
  f_log -Tn -t info "Testing ShellCheck version ... "
  f_test_shellcheck_version; f_test_result "$?" || return 1
  f_log -Tn -t info "Testing Bats version ... "
  f_test_bats_version; f_test_result "$?" || return 1

  f_log -e
  f_line -t "="

  if [[ $G_WATCH = "true" ]]; then
    f_watch
  else
    f_test
  fi

  f_line -t "="

  IFS=$original_IFS
}

if [[ "${BASH_SOURCE[0]}" == "${0}" ]]; then
  main "$@"
  exit $?
fi
