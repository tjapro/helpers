#!/usr/bin/env ./libs/bats/bin/bats

# BATS TEST
# DEV

# Loads
load './../libs/bats-support/load'
load './../libs/bats-assert/load'
load './../libs/bats-file/load'

# Source the file
source ./dev

# Filename
readonly G_TEST="DEV"

# Basic menu tests
@test "${G_TEST} - menu - Should notify invalid option" {
  run main -x
  assert_line --index 0 "Invalid option: -x"
  assert_line --index 1 "TIAGO PROGRAMMER HELPERS DEVELOPMENT"
}

# Function tests

# -h option (tests the f_usage function too)
@test "${G_TEST} - -h - Should give the usage message" {
  run main -h
  assert_line --index 0 "TIAGO PROGRAMMER HELPERS DEVELOPMENT"
  assert_line --index 1 "A simple file to help developing this project."
}

# f_compare_version
@test "${G_TEST} - f_compare_version - Compare between equal should give true" {
  run f_compare_version "1.0" "1.0"
  assert_success
}

@test "${G_TEST} - f_compare_version - Compare between major and minor should give true" {
  run f_compare_version "1.5" "1.0"
  assert_success
}

@test "${G_TEST} - f_compare_version - Compare between minor and major should give false" {
  run f_compare_version "1.0" "1.5"
  assert_failure
}

# f_test_result
@test "${G_TEST} - f_test_result - Should success" {
  run f_test_result 0
  assert_line --index 0 "ok"
}

@test "${G_TEST} - f_test_result - Should fail" {
  run f_test_result 1
  assert_line --index 0 "nok"
}

# f_install
@test "${G_TEST} - f_install - Should be created symlinks for hooks" {
  assert_symlink_to "${PWD}/.hooks/pre-commit" .git/hooks/pre-commit
}
