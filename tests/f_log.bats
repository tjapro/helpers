#!/usr/bin/env ./libs/bats/bin/bats

# BATS TEST
# F_LOG function

# Loads
load './../libs/bats-support/load'
load './../libs/bats-assert/load'
load './../libs/bats-file/load'

# Source the file
source ./functions/f_log.bash

BATSLIB_TEMP_PRESERVE_ON_FAILURE=1

# Filename
readonly G_TEST="F_LOG"

setup() {
  rm -rf "$G_FILE_AUTO"
  rm -rf "$G_FILE_TEST"
  G_FILE="test"
  G_FILE_AUTO="./logs/$(printf '%(%Y-%m-%d)T' -1).log"
  G_FILE_TEST="./logs/${G_FILE}.log"
  export G_FILE
  export G_FILE_AUTO
  export G_FILE_TEST
}

teardown() {
  if [ "$BATS_TEST_COMPLETED" ]; then
    rm -rf "$G_FILE_AUTO"
    rm -rf "$G_FILE_TEST"
  else
    printf "** Did not delete %s, as test failed **\n" "$G_FILE_AUTO"
    printf "** Did not delete %s, as test failed **\n" "$G_FILE_TEST"
  fi
}

# Basic menu tests
@test "${G_TEST} - Should notify invalid option" {
  run f_log -x
  assert_line --index 0 "Invalid option: '-x'"
  assert_line --index 1 "${G_TEST}"
}

@test "${G_TEST} - -f - Should notify required argument" {
  run f_log -f
  assert_line --index 0 "An argument is required for option '-f'"
  assert_line --index 1 "${G_TEST}"
}

@test "${G_TEST} - -t - Should notify required argument" {
  run f_log -t
  assert_line --index 0 "An argument is required for option '-t'"
  assert_line --index 1 "${G_TEST}"
}

@test "${G_TEST} - Should notify invalid argument" {
  run f_log -t "other"
  assert_line --index 0 "The argument 'other' is invalid for option '-t'"
  assert_line --index 1 "${G_TEST}"
}

# Function tests

# file existence
@test "${G_TEST} - file existence - Should create a log file" {
  run f_log
  assert_file_exist "$G_FILE_AUTO"
}

# no option
@test "${G_TEST} - no option - Should just output a default message" {
  run f_log
  assert_file_contains "$G_FILE_AUTO" "LOG     :: no message given"
}

# no option with string
@test "${G_TEST} - with argument - Should just output the required message" {
  run f_log "required message"
  assert_file_contains "$G_FILE_AUTO" "LOG     :: required message"
}

# no option, string and arguments
@test "${G_TEST} - string and arguments - Should expand the options" {
  run f_log "%s message %s" "required" "too"
  assert_file_contains "$G_FILE_AUTO" "LOG     :: required message too"
}

# -h option
@test "${G_TEST} - -h - Should give the usage message" {
  run f_log -h
  assert_line --index 0 "${G_TEST}"
  assert_line --index 1 "A simple function that writes logs in a file and/or in the shell."
}

# -d option without argument
@test "${G_TEST} - -d - Should just output nothing" {
  run f_log -d
  refute_output
}
@test "${G_TEST} - -d - Should just write nothing" {
  run f_log -d
  assert_file_empty "$G_FILE_AUTO"
}

# -d option with string
@test "${G_TEST} - -d - Should not be outputed" {
  run f_log -d "equal output"
  refute_output
}
@test "${G_TEST} - -d - Should not be writed" {
  run f_log -d "equal output"
  assert_file_empty "$G_FILE_AUTO"
}

# -e option without argument
@test "${G_TEST} - -e - Should just output nothing" {
  run f_log -e
  refute_output
}
@test "${G_TEST} - -e - Should just write nothing" {
  run f_log -e
  assert_file_empty "$G_FILE_AUTO"
}

# -e option with string
@test "${G_TEST} - -e - Should be an equal output" {
  run f_log -e "equal output"
  assert_output "equal output"
}
@test "${G_TEST} - -e - Should not be writed" {
  run f_log -e "equal output"
  assert_file_empty "$G_FILE_AUTO"
}

# -n option with string
@test "${G_TEST} - -n - string - Should just output the string without newline" {
  run f_log -n "required message"
  assert_output --partial "LOG     :: required message"
}
@test "${G_TEST} - -n - string - Should just write the string without newline" {
  run f_log -n "required message"
  assert_file_contains "$G_FILE_AUTO" "LOG     :: required message"
}

# -n option with string and arguments
@test "${G_TEST} - -n - string and arguments - Should expand and output the options without newline" {
  run f_log -n "%s message %s" "required" "too"
  assert_output --partial "LOG     :: required message too"
}
@test "${G_TEST} - -n - string and arguments - Should expand and write the options without newline" {
  run f_log -n "%s message %s" "required" "too"
  assert_file_contains "$G_FILE_AUTO" "LOG     :: required message too"
}

# -s option without argument
@test "${G_TEST} - -s - Should just output nothing" {
  run f_log -s
  refute_output
}
@test "${G_TEST} - -s - Should just write a empty message" {
  run f_log -s
  assert_file_contains "$G_FILE_AUTO" "LOG     ::"
}

# -s option with string
@test "${G_TEST} - -s - Should not be printed" {
  run f_log -s "equal output"
  refute_output
}
@test "${G_TEST} - -s - Should be an equal write" {
  run f_log -s "equal output"
  assert_file_contains "$G_FILE_AUTO" "LOG     :: equal output"
}

# -T option without argument
@test "${G_TEST} - -T - Should output a LOG default message without time" {
  run f_log -T
  assert_output "LOG     :: no message given"
}
@test "${G_TEST} - -T - Should write a LOG default message with time" {
  run f_log -T
  assert_file_contains "$G_FILE_AUTO" "LOG     :: no message given"
}

# -T option with argument
@test "${G_TEST} - -T - Should output a LOG message without time" {
  run f_log -T "equal output"
  assert_output "LOG     :: equal output"
}
@test "${G_TEST} - -T - Should output a LOG message with time" {
  run f_log -T "equal output"
  assert_file_contains "$G_FILE_AUTO" "LOG     :: equal output"
}

# -f option
@test "${G_TEST} - -f - Should create a specific log file" {
  run f_log -f "${G_FILE}"
  assert_file_exist "$G_FILE_TEST"
}

# -t option LOG
@test "${G_TEST} - -t log - Should output a LOG message" {
  run f_log -t "log"
  assert_output --partial "LOG     :: no message given"
}
@test "${G_TEST} - -t log - Should write a LOG message" {
  run f_log -t "log"
  assert_file_contains "$G_FILE_AUTO" "LOG     :: no message given"
}

# -t option INFO
@test "${G_TEST} - -t info - Should output a INFOmessage" {
  run f_log -t "info"
  assert_output --partial "INFO    :: no message given"
}
@test "${G_TEST} - -t info - Should write a INFO message" {
  run f_log -t "info"
  assert_file_contains "$G_FILE_AUTO" "INFO    :: no message given"
}

# -t option warning
@test "${G_TEST} - -t warning - Should output a WARNING message" {
  run f_log -t "warning"
  assert_output --partial "WARNING :: no message given"
}
@test "${G_TEST} - -t warning - Should write a WARNING message" {
  run f_log -t "warning"
  assert_file_contains "$G_FILE_AUTO" "WARNING :: no message given"
}

# -t option ERROR
@test "${G_TEST} - -t error - Should output a ERROR message" {
  run f_log -t "error"
  assert_output --partial "ERROR   :: no message given"
}
@test "${G_TEST} - -t error - Should write a ERROR message" {
  run f_log -t "error"
  assert_file_contains "$G_FILE_AUTO" "ERROR   :: no message given"
}
