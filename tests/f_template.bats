#!/usr/bin/env ./libs/bats/bin/bats

# BATS TEST
# F_TEMPLATE function

# Loads
load './../libs/bats-support/load'
load './../libs/bats-assert/load'

# Source the file
source ./functions/f_template.bash

# Filename
readonly G_TEST="F_TEMPLATE"

# Basic menu tests
@test "${G_TEST} - menu - Should notify invalid option" {
  run f_template -x
  assert_line --index 0 "Invalid option: '-x'"
  assert_line --index 1 "F_TEMPLATE"
}

# @test "${G_TEST} - Should notify required argument" {
#   run f_template -a
#   assert_line --index 0 "An argument is required for option '-a'"
#   assert_line --index 1 "F_TEMPLATE"
# }

# @test "${G_TEST} - Should notify invalid argument" {
#   run f_template -a "error"
#   assert_line --index 0 "The argument 'error' is invalid for option '-a'"
#   assert_line --index 1 "F_TEMPLATE"
# }

# Function tests

# no option
@test "${G_TEST} - no option - Should just output a default message" {
  run f_template
  assert_line "template :: no message given"
}

# no option with argument
@test "${G_TEST} - no option - Should just output the required message" {
  run f_template "required message"
  assert_line "template :: required message"
}

# -h option
@test "${G_TEST} - -h - Should give the usage message" {
  run f_template -h
  assert_line --index 0 "F_TEMPLATE"
  assert_line --index 1 "A simple file to show how to begin with a '.bash' library file."
}
