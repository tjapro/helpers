#!/usr/bin/env ./libs/bats/bin/bats

# BATS TEST
# F_SUDO function

# Loads
load './../libs/bats-support/load'
load './../libs/bats-assert/load'

# Source the file
source ./functions/f_sudo.bash

# Filename
readonly G_TEST="F_SUDO"

# Basic menu tests
@test "${G_TEST} - menu - Should notify invalid option" {
  run f_sudo -x
  assert_line --index 0 "Invalid option: -x"
  assert_line --index 1 "F_SUDO"
}

# Function tests

# no option without sudo
@test "${G_TEST} - no option - Without sudo should fail" {
  if [ $EUID != 0 ]; then
    run f_sudo
    assert_failure
  else
    skip
  fi
}

# no option with sudo
@test "${G_TEST} - no option - With sudo should success" {
  if [ $EUID != 0 ]; then
    skip
  else
    run f_sudo
    assert_success
  fi
}

# -h option
@test "${G_TEST} - -h - Should give the usage message" {
  run f_sudo -h
  assert_line --index 0 "F_SUDO"
  assert_line --index 1 "A simple function to ask for sudo user."
}
