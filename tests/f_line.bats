#!/usr/bin/env ./libs/bats/bin/bats

# BATS TEST
# F_LINE function

# Loads
load './../libs/bats-support/load'
load './../libs/bats-assert/load'

# Source the file
source ./functions/f_line.bash

# Filename
readonly G_TEST="F_LINE"

# Basic menu tests
@test "${G_TEST} - menu - Should notify invalid option" {
  run f_line -x
  assert_line --index 0 "Invalid option: '-x'"
  assert_line --index 1 "F_LINE"
}

@test "${G_TEST} - Should notify required argument" {
  run f_line -t
  assert_line --index 0 "An argument is required for option '-t'"
  assert_line --index 1 "F_LINE"
}

# Function tests

# no option
@test "${G_TEST} - no option - Should just output the default line" {
  run f_line
  assert_output --partial "--------"
}

# -h option
@test "${G_TEST} - -h - Should give the usage message" {
  run f_line -h
  assert_line --index 0 "F_LINE"
  assert_line --index 1 "A simple function that writes lines."
}

# -t option
@test "${G_TEST} - -t - Should give the line with *" {
  run f_line -t \*
  assert_output --partial "********"
}
