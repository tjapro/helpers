# Tiago Programmer Helpers

A simple bunch of `bash` function helpers.  
For more information, please see [CONTRIBUTING](CONTRIBUTING.md) file.

[![pipeline status](https://gitlab.com/tjapro/helpers/badges/master/pipeline.svg)](https://gitlab.com/tjapro/helpers/-/commits/master)  
[![coverage report](https://gitlab.com/tjapro/helpers/badges/master/coverage.svg)](https://gitlab.com/tjapro/helpers/-/commits/master)

## How to Use

Add this project to another with `git submodule` commands.

## How to Help

### With:

-   Issues: errors, bugs, ...
-   Ideas: new features
-   Enhancements: better code
-   Pull Requests: all of previous!

### How:

1.  Clone this project and initiate submodules: `git clone --recurse-submodules https://gitlab.com/tjapro/helpers.git`
2.  Go to the project directory.
3.  Run `./dev -i && ./dev -w`
4.  Create a branch and code your enhancement.
5.  Create the correct tests and test it.
6.  Add your entry to the `CHANGELOG.md` file.
7.  Make a Pull Request.

### Notes:

-   Follow the actual scheme of directories and files.
-   Don't forget this: a new `bash` function is a function to be tested in `bats`.
-   Help with `git submodules`: <https://stackoverflow.com/questions/1979167/git-submodule-update>

## Other

Copyright (c) 2020 Tiago Programmer  
[MIT License](LICENSE)
