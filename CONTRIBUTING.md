# Contributing to Tiago Programmer Projects

First off, thanks for taking the time to contribute!

The following is a set of guidelines for contributing to Tiago Programmer projects, which are hosted in the [Tiago Programmer](https://gitlab.com/tjapro) on GitLab. These are mostly guidelines, not rules. Use your best judgment, and feel free to propose changes to this document in a pull request.

#### Table Of Contents

[Code of Conduct](#code-of-conduct)

[What should I know before I get started?](#what-should-i-know-before-i-get-started)

[How Can I Contribute?](#how-can-i-contribute)

- [Reporting Bugs](#reporting-bugs)
- [Suggesting Enhancements](#suggesting-enhancements)
- [Your First Code Contribution](#your-first-code-contribution)
- [Pull Requests](#pull-requests)

[Styleguides](#styleguides)

## Code of Conduct

This project and everyone participating in it is governed by the [Tiago Programmer Code of Conduct](CODE_OF_CONDUCT.md). By participating, you are expected to uphold this code. Please report unacceptable behavior to [tjamadeira@gmail.com](mailto:tjamadeira@gmail.com).

## What should I know before I get started?

The Tiago Programmer repositories with a [License](LICENSE.md) of **Creative Commons Attribution-NonCommercial-NoDerivatives 4.0 International License** are personal/private.  
Those packages are exposed to maintain the transparency and help others in their works.  
_But why I cannot change it with my preferences?_ To maintain this statement: "One unique source of information".  
_But if is it necessary to change the license?_ Please, open an issue with the reasons.

## How Can I Contribute?

### Reporting Bugs

Reporting a bug is the best way to enhance these projects!

> **Note:** If you find a **Closed** issue that seems like it is the same thing that you're experiencing, open a new issue and include a link to the original issue in the body of your new one.

1. **Perform a issue search** to see if the problem has already been reported. If it has **and the issue is still open**, add a comment to the existing issue instead of opening a new one.
2. When create an issue on this repository select the `Bug Report` template and follow the instructions.

### Suggesting Enhancements

I like suggestions! Fill one if you have it!

1. **Check if there is already a bash script** that do your enhancement.
2. **Perform a issue search** to see the enhancement has already been suggested. If it has, add a comment to the existing issue instead of opening a new one.
3. When create an issue on this repository select the `Enhancement Report` template and follow the instructions.

### Your First Code Contribution

Unsure where to begin contributing to Atom? You can start by looking through these `beginner` and `help-wanted` issues:

- **Beginner issues** - issues which should only require a few lines of code, and a test or two.
- **Help wanted issues** - issues which should be a bit more involved than `beginner` issues.

Both issue lists are sorted by total number of comments. While not perfect, number of comments is a reasonable proxy for impact a given change will have.

#### Local development

Please see the [README](README.md) file.

### Pull Requests

The process described here has several goals:

- Maintain the project quality
- Fix problems that are important to users
- Engage the community in working toward the best possible script
- Enable a sustainable system for project maintainers to review contributions

Please follow these steps to have your contribution considered by the maintainers:

1. Follow all instructions in the `Pull Request` template.
2. Follow the [styleguides](#styleguides)
3. After you submit your pull request, verify that all status checks are passing <details><summary>What if the status checks are failing?</summary>If a status check is failing, and you believe that the failure is unrelated to your change, please leave a comment on the pull request explaining why you believe the failure is unrelated. A maintainer will re-run the status check for you. If we conclude that the failure was a false positive, then we will open an issue to track that problem with our status check suite.</details>

While the prerequisites above must be satisfied prior to having your pull request reviewed, the reviewer(s) may ask you to complete additional design work, tests, or other changes before your pull request can be ultimately accepted.

## Styleguides

### Bash or Bats Styleguide

Use the `template` bash or bats files and see the manner of making the documentation.

### Documentation Styleguide

Use [Markdown](https://daringfireball.net/projects/markdown).

### Directory and Files Scheme

Follow the scheme that is already programmed.
